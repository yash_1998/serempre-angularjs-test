; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('productWarranty', tinMainHeader);

    function tinMainHeader() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/product-warranty.html',
            controller: controllerFn,
            scope: {
                productData: '='
            },
            link: function ($scope, element) {
            }
        };

        function controllerFn($scope) {
            $scope.hoverIndex = null;
            $scope.selectedWarranty = {};

            $scope.elementHover = (index) => {
                $scope.hoverIndex = index;
            }

            $scope.elementHoverLeave = () => {
                $scope.hoverIndex = null;
            }

            $scope.getOuterBorderClass = (index, item) => {
                console.log(index == $scope.hoverIndex, item, index)
                if (index == $scope.hoverIndex) {
                    return { 'hover-element-border': true };
                } else if (item.id == $scope.selectedWarranty.id) {
                    return { 'selected-element-border': true };
                }
            }

            $scope.getInnerBorderClass = (index, item) => {
                if (index == $scope.hoverIndex && item.id == $scope.selectedWarranty.id) {
                    return { 'selected-element-border': true };
                }

            }

            $scope.selectWarranty = (item) => {
                $scope.selectedWarranty = item;
            }
        }


        return directiveDefinitionObject;
    }

})();