; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('productFeatures', tinMainHeader);

    function tinMainHeader() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/product-features.html',
            controller: controllerFn,
            scope: {
                productData: '='
            },
            link: function ($scope, element) {
            }

        };


        function controllerFn($scope) {

            $scope.hoverIndex = null;
            $scope.selectedFeatures = {};

            $scope.elementHover = (index) => {
                $scope.hoverIndex = index;
            }

            $scope.elementHoverLeave = () => {
                $scope.hoverIndex = null;
            }

            $scope.getOuterBorderClass = (index, item) => {
                if (index == $scope.hoverIndex) {
                    return { 'hover-element-border': true };
                } else if (item.id == $scope.selectedFeatures.id) {
                    return { 'selected-element-border': true };
                }
            }

            $scope.getInnerBorderClass = (index, item) => {
                if (index == $scope.hoverIndex && item.id == $scope.selectedFeatures.id) {
                    return { 'selected-element-border': true };
                }

            }

            $scope.selectFeature = (item) => {
                $scope.selectedFeatures = item;
            }
        }

        return directiveDefinitionObject;
    }

})();