; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('productColors', tinMainProductTabs);

    function tinMainProductTabs() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/product-colors.html',
            controller: controllerFn,
            scope: {
                productData: '='
            },
            link: function ($scope, element) {
            }

        };

        function controllerFn($scope) {
            $scope.selectedColor = {};
            $scope.hoverIndex = null
            $scope.selectColor = (item) => {
                $scope.selectedColor = item;
            }

            $scope.elementHover = (index) => {
                $scope.hoverIndex = index;
            }

            $scope.elementHoverLeave = () => {
                $scope.hoverIndex = null;
            }


            $scope.getOuterBorderClass = (index, item) => {
                if (index == $scope.hoverIndex) {
                    return { 'hover-element-border': true };
                } else if (item.id == $scope.selectedColor.id) {
                    return { 'selected-element-border': true };
                }
            }

            $scope.getInnerBorderClass = (index, item) => {
                if (index == $scope.hoverIndex && item.id == $scope.selectedColor.id) {
                    return { 'selected-element-border': true };
                }

            }
        }

        return directiveDefinitionObject;
    }

})();