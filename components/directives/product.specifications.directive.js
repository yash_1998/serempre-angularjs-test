; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('productSpecifications', tinMainHeader);

    function tinMainHeader() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/product-specifications.html',
            controller: controllerFn,
            scope: {
                productData: '='
            },
            link: function ($scope, element) {
            }
        };

        function controllerFn($scope) {
            $scope.getLabelName = (item) => {
                if (item === 'dimension') {
                    return 'Dimension'
                } else if (item === 'frequency_response') {
                    return 'Frequency Response';
                } else if (item === 'frequency_response_microphone') {
                    return 'Frequency Response Microphone'
                } else if (item === 'noise_cancelation') {
                    return 'Noise Cancelation'
                } else if (item === 'power_suply') {
                    return 'Power supply'
                } else if (item === 'usb_standard') {
                    return 'Usb Standard'
                }
            }



        }

        return directiveDefinitionObject;
    }

})();