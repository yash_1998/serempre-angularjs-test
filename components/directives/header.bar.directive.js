; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('headerBar', tinMainHeader);

    function tinMainHeader() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/header-bar.html'
        };

        return directiveDefinitionObject;
    }

})();