; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('productTabs', tinMainProductTabs);

    function tinMainProductTabs() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/product-tabs.html',
            controller: controllerFn,
            scope: {
                productData: '='
            },
            link: function ($scope, element) {
            }

        };


        function controllerFn($scope, QueryService) {
            $scope.selectedIndex = 0;

            $scope.setIndex = (value) => {
                $scope.selectedIndex = value;
            }
        }

        return directiveDefinitionObject;
    }

})();