; (function () {

    'use strict';

    /**
     * Main navigation, just a HTML template
     * @author Jozef Butko
     * @ngdoc  Directive
     *
     * @example
     * <main-nav><main-nav/>
     *
     */
    angular
        .module('serempre')
        .directive('productTechnicalDetails', tinMainHeader);

    function tinMainHeader() {

        // Definition of directive
        var directiveDefinitionObject = {
            restrict: 'E',
            templateUrl: 'components/directives/product-technical-details.html',
            scope: {
                productData: '='
            },
            link: function ($scope, element) {
            }
        };

        return directiveDefinitionObject;
    }

})();