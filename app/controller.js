/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 * 
 */
; (function () {

  angular
    .module('serempre')
    .controller('MainController', MainController);

  MainController.$inject = ['LocalStorage', 'QueryService', '$scope'];


  function MainController(LocalStorage, QueryService, $scope) {

    // 'controller as' syntax
    var self = this;
    $scope.productData = {};


    function getProductData() {
      QueryService.query('GET', 'items', {}, {}).then(function (response) {
        if (response && response.data) {
          $scope.productData = response.data[0];
          if ($scope.productData) {
            $scope.selectedImage = $scope.productData.images[0];
          }
        }
      })
    }

    getProductData();



    ////////////  function definitions


    /**
     * Load some data
     * @return {Object} Returned object
     */
    // QueryService.query('GET', 'posts', {}, {})
    //   .then(function(ovocie) {
    //     self.ovocie = ovocie.data;
    //   });
  }


})();